package com.innogrant.connector.fillforms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Updatexlsx {

	public Updatexlsx() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws Exception {
		// Read Excel document first
		FileInputStream input_document = new FileInputStream(new File(
				"C:/Users/Udit/Desktop/premiumcalculation.xlsx"));
		System.out.println("Input document" + input_document);
		// convert it into a POI object
		XSSFWorkbook my_xlsx_workbook = new XSSFWorkbook(input_document);
		System.out.println("Xlsx workbook" + my_xlsx_workbook);
		// Read excel sheet that needs to be updated
		XSSFSheet my_worksheet = my_xlsx_workbook.getSheetAt(0);
		System.out.println("Sheet Name" + my_worksheet.getSheetName());
		// System.out.println("Work sheet name=="+my_worksheet);
		// declare a Cell object
		// Access the cell first to update the value
		// XSSFRow row = my_worksheet.getRow(5);//.getCell(2);
		// Iterator<Cell> iterator= row.cellIterator();
		CellReference crb5 = new CellReference("B5");
		XSSFRow rowb5 = my_worksheet.getRow(crb5.getRow());
		XSSFCell cellb5 = rowb5.getCell(crb5.getCol());
		cellb5.setCellValue(20000.00);
		System.out.println("Value====B5==" + cellb5.getNumericCellValue());
		CellReference crb6 = new CellReference("B6");
		XSSFRow rowb6 = my_worksheet.getRow(crb6.getRow());
		XSSFCell cellb6 = rowb6.getCell(crb6.getCol());
		System.out.println("Value====B6==" + cellb6.getStringCellValue());
		CellReference crb7 = new CellReference("B7");
		XSSFRow rowb7 = my_worksheet.getRow(crb7.getRow());
		XSSFCell cellb7 = rowb7.getCell(crb7.getCol());
		System.out.println("Value====B7==" + cellb7.getStringCellValue());
		CellReference crb8 = new CellReference("B8");
		XSSFRow rowb8 = my_worksheet.getRow(crb8.getRow());
		XSSFCell cellb8 = rowb8.getCell(crb8.getCol());
		System.out.println("Value====B8==" + cellb8.getNumericCellValue());
		CellReference crb9 = new CellReference("B9");
		XSSFRow rowb9 = my_worksheet.getRow(crb9.getRow());
		XSSFCell cellb9 = rowb9.getCell(crb9.getCol());
		System.out.println("Value====B9==" + cellb9.getStringCellValue());
		System.out.println("Premium Value before changes=="
				+ getPremiumValue(my_worksheet));
		// my_xlsx_workbook.getCreationHelper().createFormulaEvaluator()
		// .evaluateAll();
		// my_xlsx_workbook.setForceFormulaRecalculation(true);
		// XSSFFormulaEvaluator.evaluateAllFormulaCells(my_xlsx_workbook);
		XSSFFormulaEvaluator.evaluateAllFormulaCells(my_xlsx_workbook);

		// new XSSFFormulaEvaluator(my_xlsx_workbook).evaluateAll();
		System.out.println("Premium Value after changes=="
				+ getPremiumValue(my_worksheet));
		input_document.close();
		// while(iterator.hasNext()) {
		// Object element = iterator.next();
		// System.out.print(element + " ");
		// }

		// System.out.println("Coloumn Index====="+cell.getColumnIndex());
		// // Get current value and reduce 5 from it
		// System.out.println("Cell Current value===="+cell.getNumericCellValue());
		// // cell.setCellValue(15000);
		// System.out.println("Cell Numeric value===="+cell.getNumericCellValue());
		// important to close InputStream

		// //Open FileOutputStream to write updates
		// FileOutputStream output_file =new FileOutputStream(new
		// File("C:/Users/Udit/Desktop/premiumcalculation.xlsx"));
		// //write changes
		// my_xlsx_workbook.write(output_file);
		// //close the stream
		// output_file.close();

	}

	private static double getPremiumValue(XSSFSheet my_worksheet) {
		CellReference crg11 = new CellReference("G11");
		XSSFRow rowg11 = my_worksheet.getRow(crg11.getRow());
		XSSFCell cellg11 = rowg11.getCell(crg11.getCol());
		// System.out.println("Value====G11==" + cellg11.getNumericCellValue());
		return cellg11.getNumericCellValue();
	}
}
