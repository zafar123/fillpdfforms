package com.innogrant.connector.fillforms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class PdfFormsWriter {

//	public static final String SRC = "E:/pdf/input/FR Cardoen Polis.pdf";
//	public static final String DEST = "E:/pdf/output/Update_New_FR Cardoen Polis.pdf";
	
	//public static final String SRC = "D:\\Zafar\\Product Changes as per mail\\Cardoen pdf file for 2019\\NL Cardoen vervaldagbericht.pdf";
	//public static final String DEST = "D:\\Zafar\\Product Changes as per mail\\Cardoen pdf file for 2019\\Update_NL Cardoen vervaldagbericht.pdf";
	
	public static final String SRC = "D:\\Savita\\carinsurance\\FillPdfForms\\Input\\AV FR Verzekeringsfiche.pdf";
	public static final String DEST = "D:\\Savita\\carinsurance\\FillPdfForms\\Output\\AV FR Verzekeringsfiche_2_output.pdf";

	public static void main(String[] args) throws DocumentException,
			IOException {
		File file = new File(DEST);
		file.getParentFile().mkdirs();
		new PdfFormsWriter().manipulatePdf(SRC, DEST);
	}

	public void manipulatePdf(String src, String dest)
			throws DocumentException, IOException {
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));

		AcroFields form = stamper.getAcroFields();
		 Map<String, Item> fi = form.getFields();
		 
		 System.out.println(fi.size());
		 Set<Map.Entry<String, Item>> entrySet = fi.entrySet();
		 int i = 0;
		 for (Entry entry : entrySet) {
		i++;		 
		 
		 String key = entry.getKey().toString();
		 form.setField(key, String.valueOf(i));
		 //form.setField(key, key);
		 System.out
		 .println("------------------------------------------------");
		 System.out.println("key: " + entry.getKey() + " value: "
		 + i);
		
		 
		 System.out.println("=============================================");
		 String[] values = form.getAppearanceStates(key);
		   for (String value : values) {
		       System.out.println(value);
		    }
		System.out.println("=============================================");
		 if(key.contains("Ch"))
		 {
		 form.setField(key, "Yes");
		
		 
		// form.setField(key, "2");
		 }
		
		 }
//		 String arr[] = form.getAppearanceStates("Check Box 35");
//		 for (int i = 0; i < arr.length; i++) {
//		 System.out.println(arr[i]);
//		 }
//		form.setField("naam", "	Jhon Smith");
//		form.setField("naam4", "	2415 Etterbeek");
//		form.setField("naam5", "	Belgium");
//		form.setField("naam2", "	Jhon Smith");
//		form.setField("301a", "	Jhon Smith");
//		form.setField("301", "	Smith");
//		form.setField("302", "	Jhon");
//		form.setField("303", "	241");
		// form.setField("304", "	30");
//		form.setField("305", "	Brussels Airport");
//		form.setField("306", "	1040");
//		form.setField("307", "	Etterbeek");
//		form.setField("308", "	02 733 6781");
//		form.setField("309", "	0800 81 345");
//		form.setField("310", "	jhonsmith@gmail.com");
//		form.setField("311", "	01-01-1991");
//		form.setField("312", "	Audi 7");
//		form.setField("313", "	4 wheeler");
//		form.setField("314", "	1QRCJ456");
//		form.setField("315", "	88ABCD");
		// form.setField("316", "	-----------");
//		form.setField("317", "	2014");
//		form.setField("318", "	08-05-2014");
		// form.setField("319", "	1400M");
		// form.setField("320", "	542");
		// form.setField("321", "	Audi");
		// form.setField("Check Box 30", "Yes");// Page 5 first check box
		// form.setField("Check Box 31", "Yes");// Page 5 second check box
		// form.setField("Check Box 32", "Yes");// Page 5 third check box
		// form.setField("Check Box 33", "Yes");// Page 5 fourth check box
		// form.setField("Check Box 34", "Yes");// Page 5 fifth check box
		// form.setField("Check Box 35", "Yes");// Page 5 six check box
		// form.setField("Check Box 36", "Yes");// Page 5 seventh check box
		// form.setField("Check Box 37", "Yes");// Page 5 eightth check box
		// form.setField("Check Box 38", "Yes");// Page 5 ninth check box
		// form.setField("Check Box 39", "Yes");// Page 5 tenth check box
		// form.setField("Check Box 20", "Yes");// Page 4 1st check box
		// form.setField("Check Box 21", "Yes");// Page 4 2nd check box
		// form.setField("Check Box 22", "Yes");// Page 4 3rd check box
		// form.setField("Check Box 6", "Yes");// Page 1 1st check box
		// form.setField("Check Box 7", "Yes");// Page 1 2nd check box
//		form.setField("Check Box 8", "Yes");// Page 3,6 1st check box
		// form.setField("Check Box 10", "Yes");// Page 3 6th check box
		// form.setField("Check Box 11", "Yes");// Page 3,6 2nd check box
		// form.setField("Check Box 12", "Yes");// Page 3 4th check box
		// form.setField("Check Box 13", "Yes");// Page 3 5th check box
//		form.setField("Check Box 14", "Yes");// Page 3 3rd check box
//		form.setField("Check Box 15", "Yes");// Page 3 7th check box
		// form.setField("Check Box 16", "Yes");// /Page 3 8th check box
		// form.setField("Check Box 39n", "Yes");// Page 5 11th check box
//		form.setField("41", "	John Smith");
//		form.setField("42", "	20000");
//		form.setField("43", "	Yes");
//		form.setField("44", "	Yes");
//		form.setField("45", "	552.89");
//		form.setField("46", "	82.80");
//		form.setField("47", "	20.00");
//		form.setField("48", "	12.50");
//		form.setField("49", "	501.70");
//		form.setField("56", "	09-05-2017");
//		form.setField("611", "	Smith");
//		form.setField("612", "	Jhon");
//		form.setField("613", "	241");
		// form.setField("614", "	30");
//		form.setField("615", "	Brussels Airport");
//		form.setField("616", "	1040");
//		form.setField("617", "	Etterbeek");

		// form.setField("3", "	XYZ");
		// form.setField("3", "	XYZ");
		// form.setField("3", "	XYZ");
		// form.setField("3", "	XYZ");
		// form.setField("3", "	XYZ");
		// form.setField("3", "	XYZ");

		stamper.setFormFlattening(true);
		stamper.close();
	}
}
